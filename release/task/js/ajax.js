function Ajax() {
  
  let logger = console;
  
  let service = {
      RequestType: {
        Post: 'POST',
        Get: 'GET'
      },
      
      checkProjectNameExist: checkProjectNameExist,
      updateTaskStatus: updateTaskStatus,
      
      foo: () => logger.info('FOOOO')
  };
  
  function checkProjectNameExist(projectName) {
    return sendRequest('/api/v1/chk_proj_name_exist', null, 'POST');
  }
  
  function updateTaskStatus(task) {
    logger.info('Gogint to send update task status. TaskId -> '+ task.id 
        +'; New Status -> ' + task.status);
     // return sendRequest('http://localhost:8080/task/api/v1?cmd=update-task-status', task, 'POST');
    return sendRequest('./api/v1?cmd=update-task-status', task, 'POST');
    
  }
  
  function sendRequest(url, data, method) {
    
    method = method || 'POST';
    
    let promise = new Promise(function(resolve, reject) {
      
      let params = {
        headers: { 
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        url: url,
        method: method,
        dateType: 'json'
      };
      
      if(data) {
        let dataFormatted = data;
        if(typeof data !== 'string') {
          dataFormatted = JSON.stringify(data);
        }
        
        params.data = dataFormatted;
      }
      
      $.ajax(params)
      .done((response) => {
         resolve(response);
      }).fail(err => {
        reject(err);
      });
    });
    
    return promise;
    
  }
  
  function loadDataNonJquery(url, onLoaded, onError) {
    let xhr = new XMLHttpRequest();

    xhr.open('post', url);

    xhr.onreadystatechange = function() {

      logger.debug(`[ajax][xhr] OnReadyStateChange. ReadyState -> ${this.readyState} & Status -> ${this.status}`);

      if (this.readyState === 4 && this.status === 200) {
        let respObj = JSON.parse(this.responseText);
        onLoaded(respObj);
      }
    };

    xhr.addEventListener('error', onError);
    xhr.send();
  }
  
  return service;
}