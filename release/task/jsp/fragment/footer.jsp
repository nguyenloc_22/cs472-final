<!-- Copyright -->
<div class="footer-copyright text-center py-2 bg-dark">
  <div class="row">
    <div class="col-sm-5 col-xs-12">
      <span>� 2019 Copyright: <a href="#"> CS472 - WAP</a></span>
    </div>
    
    <div class="col-sm-7 col-xs-12">
      <span>Hit Counter: ${ hitCounter } | Total Hit Counter: ${ totalHitCounter }</span>
    </div>
  </div>
</div>
<!-- Copyright -->