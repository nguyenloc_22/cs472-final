<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="description" content="CS472-WAP" />
<meta name="keywords" content="HTML, CSS" />

<link
  href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css"
  rel="stylesheet"
  integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx"
  crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
<link
  href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css"
  rel="stylesheet" />

<title>CS472-WAP ::: Task management!</title>
</head>
<body>
  <header>
    <%@ include file="../fragment/header.jsp"%>
  </header>

  <div class="container page-content" style="max-width: 720px">
    <h1 class="my-4">Add Task</h1>
    <form action="task-add" method="post">
      <div class="form-group">
        <label for="proid">Project Name:</label> <input type="text"
          class="form-control" id="proid" name="projectName"
          value="${project.name}" readonly> <input type="hidden"
          name="projectId" value="${project.id}">
      </div>
      <div class="form-group">
        <label for="taskName">Task name:</label> <input type="text"
          class="form-control" id="taskName" name="taskName" required>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="enddate">Select Start Date:</label> <input
              placeholder="Selecte Start date" type="date"
              name="startdate" value="" id="startdate"
              class="form-control datepicker">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="enddate">Select End Date:</label> <input
              placeholder="Selecte end date" type="date" name="enddate"
              id="enddate" class="form-control datepicker">
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="desc">Task Description:</label>
        <textarea class="form-control" rows="5" id="desc" name="desc"
          required></textarea>
      </div>

      <div class="form-group">
        <label for="adduser">Add user:</label> <select
          class="form-control add-multi-user" name="users"
          multiple="multiple" id="adduser" required>
          <option value="">Select User</option>
          <c:forEach items="${users}" var="user">
            <option value="${user.id}">${user.username}</option>
          </c:forEach>
        </select>
      </div>
      <input type="submit" value="Add Task" class="btn btn-default btn-primary">
    </form>
    <br/>
  </div>

  <footer class="page-footer font-small blue bg-primary">
    <%@ include file="../fragment/footer.jsp"%>
  </footer>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script
    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
  <script src="js/page-task-add.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
