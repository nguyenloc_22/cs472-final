package edu.mum.cs.cs472.task_mgnt.type;

public enum ProjectStatus {
	New(1),
	InProcess(2),
	Finished(3),
	Closed(4),
	Canceled(5);
	
	private int value;
	
	ProjectStatus(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
