package edu.mum.cs.cs472.task_mgnt.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class AuthenticationFilter
 */
@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthenticationFilter() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		//convert Http request
		HttpServletRequest httpRequest = (HttpServletRequest) request; 
		HttpSession session = httpRequest.getSession();
		
		//decode uri, if uri is /login then pass
		String[] uri = httpRequest.getRequestURI().split("/");
		
		String route = uri[uri.length - 1];
		request.setAttribute("currentRoute", route);
		if (route.equals("login") || route.equals("register")) {
			chain.doFilter(request, response);
		} else {
			
			//get userId from session
			Long userId = (Long) session.getAttribute("user");
			
			//if null redirect to login form
	        if (userId == null) {
	        	HttpServletResponse httpResponse = (HttpServletResponse) response;
	        	httpResponse.sendRedirect("login");
	        } else {
	        	//pass the filter
	        	chain.doFilter(request, response);
	        }
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
