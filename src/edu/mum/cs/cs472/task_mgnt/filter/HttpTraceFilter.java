package edu.mum.cs.cs472.task_mgnt.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import edu.mum.cs.cs472.task_mgnt.util.Logger;

@WebFilter(filterName="HttpTraceFilter", urlPatterns={"*"})
public class HttpTraceFilter implements Filter {

	private static final boolean traceRequest = false;
	private static Logger logger;
	
	static {
		logger = Logger.getLogger(HttpTraceFilter.class.getCanonicalName());
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
//		Filter.super.init(filterConfig);
		
	}
	
	@Override
	public void doFilter(
		ServletRequest request, 
		ServletResponse response, 
		FilterChain chain)
		throws IOException, ServletException {
		
		traceRequest((HttpServletRequest)request);
		
		// pass the request along the filter chain
		chain.doFilter(request, response);
		// System.out.println("After the servlet");
	}

	@Override
	public void destroy() {

	}

	private void traceRequest(HttpServletRequest req) {
		if(!traceRequest) {
			return;
		}
		
		String ctxPath = req.getContextPath();
		String localAddr = req.getLocalAddr();
		String pathInfo = req.getPathInfo();
		String protocol = req.getProtocol();
		String reqUri = req.getRequestURI();
		
		StringBuilder sb = new StringBuilder();
		sb.append("\nContextPath: ").append(ctxPath);
		sb.append("\nLocalAddr: ").append(localAddr);
		sb.append("\nPathInfo: ").append(pathInfo);
		sb.append("\nProtocol: ").append(protocol);
		sb.append("\nReqUrl: ").append(reqUri);
		
		logger.debug(sb.toString());
	}

}
