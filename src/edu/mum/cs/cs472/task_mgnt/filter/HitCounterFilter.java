package edu.mum.cs.cs472.task_mgnt.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(filterName="HitCounterFilter", urlPatterns={"*"})
public class HitCounterFilter implements Filter {

	private static long hitCounter = 0;
	private static Map<String, Long> hitCounterPerPage = new HashMap<String, Long>();
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
//		Filter.super.init(filterConfig);
		
	}
	
	@Override
	public void doFilter(
		ServletRequest request, 
		ServletResponse response, 
		FilterChain chain)
		throws IOException, ServletException {
		
		String method = "";
		if(request instanceof HttpServletRequest) {
			HttpServletRequest httpSReq = (HttpServletRequest) request;
			method = httpSReq.getMethod();
			
			if("get".equals(method.toLowerCase())) {
				hitCounter ++;
				
				String path = httpSReq.getContextPath();
				Long counter = hitCounterPerPage.get(path);
				if(counter == null) {
					counter = 0L;
					hitCounterPerPage.put(path, counter);
				}
				counter += 1;
			}
		}
		
		// System.out.println("Before the servlet -> HitCounter: " + hitCounter);
		// request.setAttribute("totalHitCounter", hitCounter);
		
		// Hit Counter can be stored within Servlet Context
		// This storage is available across requests
		request.getServletContext().setAttribute("totalHitCounter", hitCounter);
		
		// pass the request along the filter chain
		chain.doFilter(request, response);
		// System.out.println("After the servlet");
	}

	@Override
	public void destroy() {

	}

}
