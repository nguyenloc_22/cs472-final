package edu.mum.cs.cs472.task_mgnt.custom_tag;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.type.TaskStatus;

public class TaskStardEndCustomTag extends SimpleTagSupport  {
	
	private static DateTimeFormatter dtf;
	
	static {
		dtf = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm");
	}

	private Task task;
	
	public void setTask(Task task) {
		this.task = task;
	}
	
	public Task getTask() {
		return task;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		super.doTag();
		
       JspWriter out = getJspContext().getOut();
       
       StringBuilder sb = new StringBuilder();
       LocalDateTime n = LocalDateTime.now();
       boolean isExpired = n.isAfter(task.getEndDate());
       
       isExpired = 
		   isExpired && 
		   (task.getStatus() != TaskStatus.Finished.getValue());
       
       sb.append("<div class='card-text task-start-end'>");
       sb.append("<div><b>Start:</b> "+ dtf.format(task.getStartDate()) +"</div>");
       sb.append("<div><b>End:</b> "+ dtf.format(task.getEndDate()) +"</div>");
       if(isExpired) {
    	   sb.append("<div class='warn-blink'><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i></div>");
       }
       sb.append("</div>");
       
       out.println(sb.toString());

	}	
}
