package edu.mum.cs.cs472.task_mgnt.service;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.Task;

public interface ITaskService {
	Task getTaskById(Long taskId);
	List<Task> getTasksByProject(Long projectId);
	List<Task> getTasksByUserAndProject(Long userId, Long projectId);
	List<Task> getAllTasks();
	Task addTask(Task task, List<Long> users);
	Task updateTask(Task task);
	Task deleteTask(Long taskId);
}
