package edu.mum.cs.cs472.task_mgnt.service;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.Contact;

public interface IContactService {
	
	Contact addContact(Contact contact);
	List<Contact> getAllContacts();
}
