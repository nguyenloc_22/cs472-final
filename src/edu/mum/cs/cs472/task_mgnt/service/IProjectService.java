package edu.mum.cs.cs472.task_mgnt.service;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.Project;
import edu.mum.cs.cs472.task_mgnt.model.User;

public interface IProjectService {
	
	boolean checkProjectNames(String projectName);
	
	/**
	 * Get project by id
	 * @param projectId
	 * @return
	 */
	Project getProjectById(Long projectId);
	
	/**
	 * Get all available projects from db
	 * @return
	 */
	List<Project> getAllProjects();
	
	/**
	 * Add a project into db.
	 * @param project
	 * @return
	 */
	Project addProject(Project project, List<Long> user);
	
	/**
	 * Update a project.
	 * @param project
	 * @return
	 */
	Project updateProject(Project project);
	
	/**
	 * Delete a project by it id
	 * If project doesn't exist (check by it's id), an
	 * exception shall be thrown
	 * @param projectId
	 */
	void deleteProject(Long projectId);
	
	/**
	 * Get all users from a specific project.
	 * @param projectId
	 * @return list of project involving into project
	 */
	List<User> getUsersFromProject(Long projectId);
	
	Project getProjectByName(String projectName);

}
