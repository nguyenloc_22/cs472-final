package edu.mum.cs.cs472.task_mgnt.service.impl;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.dao.TodoDAO;
import edu.mum.cs.cs472.task_mgnt.model.Todo;
import edu.mum.cs.cs472.task_mgnt.service.ITodoService;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class TodoServiceImpl implements ITodoService {
	
	private Logger logger;
	private TodoDAO todoDao;
	
	public TodoServiceImpl() {
		todoDao = new TodoDAO();
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.info("service-initialization -> " + this.getClass().getSimpleName());
	}

	@Override
	public Todo getTodoById(Long todoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Todo> getAllTodos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Todo addTodo(Todo todo) {
		try {
			return todoDao.add(todo);
		} catch (Exception e) {
			
		}
		
		return null;
	}

	@Override
	public Todo updateTodo(Todo todo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Todo deleteTodo(Long todoId) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
