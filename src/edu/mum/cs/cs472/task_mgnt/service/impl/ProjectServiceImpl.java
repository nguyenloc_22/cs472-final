package edu.mum.cs.cs472.task_mgnt.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import edu.mum.cs.cs472.task_mgnt.dao.ProjectDAO;
import edu.mum.cs.cs472.task_mgnt.dao.UserDAO;
import edu.mum.cs.cs472.task_mgnt.dao.UserProjectDAO;
import edu.mum.cs.cs472.task_mgnt.model.Project;
import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.model.UserProject;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class ProjectServiceImpl implements IProjectService {
	
	private ProjectDAO projectDao;
	private UserDAO userDao;
	private UserProjectDAO userProjectDao;
	private Logger logger;
	
	public ProjectServiceImpl() {
		projectDao = new ProjectDAO();
		userDao = new UserDAO();
		userProjectDao = new UserProjectDAO();
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.debug("ProjectService initialized!");
	}
	
	@Override
	public boolean checkProjectNames(String projectName) {
		Project project = getProjectByName(projectName);
		if(project != null) return true;
		else return false;
	}
	
	@Override
	public List<User> getUsersFromProject(Long projectId) {
		List<UserProject> userProjects = userProjectDao.findByProject(projectId);
		if(userProjects == null || userProjects.isEmpty()) {
			return null;
		}
		
		List<Long> userIds = userProjects.stream()
		.map(UserProject::getUserId)
		.collect(Collectors.toList());
		
		try {
			return userDao.findByIds(userIds);
		} catch (Exception e) {
			logger.error("Failed to exeucte findByIds", e);
			return null;
		}

	}

	@Override
	public Project getProjectById(Long projectId) {
		try {
			return projectDao.findById(projectId);	
		} catch (Exception e) {
			logger.error("An error occurs while getting project by id.", e);
			return null;
		}
		
	}

	@Override
	public List<Project> getAllProjects() {
		try {
			return projectDao.findAll();	
		} catch(Exception e) {
			logger.error("An error occurs while getting all projects.", e);
			return null;
		}
		
	}

	@Override
	public Project addProject(Project project, List<Long> users) {
		try {
			Project p = projectDao.add(project);
			users.forEach(user -> {
				userProjectDao.add(new UserProject(p.getId(), user));
			});
			return p;
		} catch(Exception e) {
			logger.error("An error occurs while adding project.", e);
			return null;
		}
		
	}

	@Override
	public Project updateProject(Project project) {
		try {
			projectDao.update(project);	
		} catch(Exception e) {
			logger.error("An error occurs while updating project.", e);
			return null;
		}
		
		return project;
	}

	@Override
	public void deleteProject(Long projectId) {
		try {
			projectDao.delete(projectId);	
		} catch(Exception e) {
			logger.error("An error occurs while deleting project.", e);
		}
	}

	@Override
	public Project getProjectByName(String projectName) {
		try {
			List<Project> projects = projectDao.findByColumn("name", projectName);
			if(projects != null && !projects.isEmpty()) {
				return projects.get(0);
			}
			
			return null;
		} catch(Exception e) {
			logger.error("An error occurs while get project by name.", e);
			return null;
		}
	}

}
