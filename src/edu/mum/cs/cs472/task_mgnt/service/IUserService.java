package edu.mum.cs.cs472.task_mgnt.service;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.User;

public interface IUserService {
	boolean login(User user);
	
	User addUser(User user) throws Exception;
	User updateUser(User user) throws Exception;
	User getUserById(Long userId) throws Exception;
	List<User> getAllUsers();
	void deleteUser(Long userId);
	List<User> getUsersByTaskId(Long taskId);
	User login(String username, String password);
}
