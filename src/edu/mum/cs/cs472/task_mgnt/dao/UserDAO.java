package edu.mum.cs.cs472.task_mgnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.User;

public class UserDAO extends AbstractDAO<User, Long> {
	
	public User findByUserName(String username) throws SQLException {
		
		List<User> users = findByColumn("userName", username);
		if(users != null && !users.isEmpty()) {
			return users.get(0);
		}
		
		return null;
	}
	
	public List<Long> findUserIdByProjectId(Long projectId) throws SQLException {
		Connection connection = null;
		String query = "select userId from user_project projectId = " + projectId + ";";
		try {
			connection = dataSource.getConnection();
			PreparedStatement pstmt = connection.prepareStatement(query);
	        ResultSet rs = pstmt.executeQuery();
	        List<Long> listUserId = new ArrayList<Long>();
	        while(rs.next()) {
	        	Long id = (Long) rs.getObject("userId");
	        	listUserId.add(id);
	        }
	        return listUserId;
		} catch(SQLException e) {
			System.out.println(e);
			return null;
		} finally {
			tryCloseConnection(connection);
		}
	}
}
