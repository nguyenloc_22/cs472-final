package edu.mum.cs.cs472.task_mgnt.dao;

import java.sql.SQLException;
import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.UserProject;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class UserProjectDAO extends AbstractDAO<UserProject, Long> {
	
	private Logger logger;
	
	public UserProjectDAO() {
		logger = Logger.getLogger(this.getClass().getCanonicalName());
	}
	
	public List<UserProject> findByProject(long projectId) {
		
		try {
			return findByColumn("projectId", projectId);
		} catch (SQLException e) {
			logger.error("Failed to get UserProjects by projectId", e);
			return null;
		}

	}
}
