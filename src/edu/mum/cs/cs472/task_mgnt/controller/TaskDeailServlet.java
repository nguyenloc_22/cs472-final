package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.model.Project;
import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.service.ITaskService;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;

/**
 * Servlet implementation class TaskDeail
 */
@WebServlet("/taskdetail")
public class TaskDeailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ITaskService taskService;
	private IProjectService projectService;
	private IUserService userService;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskDeailServlet() {
        super();
        projectService = ServiceFactory.getInstance().getProjectService();
        taskService = ServiceFactory.getInstance().getTaskService();
        userService = ServiceFactory.getInstance().getUserService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Task
		Long taskId = Long.parseLong(request.getParameter("id"));	
		Task task = taskService.getTaskById(taskId);
		if(task==null) {
			response.sendRedirect("project");
		}else {
			Project project = projectService.getProjectById(task.getProjectId());
			List<Task> alltask = taskService.getTasksByProject(task.getProjectId());
			List<User> users = userService.getUsersByTaskId(task.getId());
			request.setAttribute("task", task);
			request.setAttribute("project", project);
			request.setAttribute("alltask", alltask);
			request.setAttribute("users", users);
			response.getWriter().append("Served at: ").append(request.getContextPath());
			String targetJspPath = "/jsp/page/taskdetail.jsp";
			
			request
	        .getRequestDispatcher(targetJspPath)
	        .forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
