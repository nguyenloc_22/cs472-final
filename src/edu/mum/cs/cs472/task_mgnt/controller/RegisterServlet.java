package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/register")
public class RegisterServlet extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	
	private IUserService userService;
	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        userService = ServiceFactory.getInstance().getUserService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {
		
		super.doGet(request, response);
		String targetJspPath = "/jsp/page/signup.jsp";
		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		LocalDate dob = LocalDate.parse(request.getParameter("dob"));
		LocalDateTime dob2 = dob.atStartOfDay();
		User user = new User(username, password, firstName, lastName, "", dob2);
		try {
			userService.addUser(user);
			response.sendRedirect("./login");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
