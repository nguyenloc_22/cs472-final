package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.model.Project;
import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.service.ITaskService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;
import edu.mum.cs.cs472.task_mgnt.type.TaskStatus;

/**
 * Servlet implementation class TaskAddServlet
 */
@WebServlet("/task-add")
public class TaskAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IProjectService projectService;
	private ITaskService taskService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskAddServlet() {
        super();
        projectService = ServiceFactory.getInstance().getProjectService();
        taskService = ServiceFactory.getInstance().getTaskService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		long projectId = Long.parseLong(request.getParameter("projectId"));
		
		Project project = projectService.getProjectById(projectId);
		if (project == null) {
			response.sendRedirect("project");
		} else {
			List<User> users = projectService.getUsersFromProject(projectId);
			request.setAttribute("project", project);
			request.setAttribute("users", users);
			String targetJspPath = "/jsp/page/task-add.jsp";
			
			request
	        .getRequestDispatcher(targetJspPath)
	        .forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String projectId = request.getParameter("projectId");
		String taskDescription = request.getParameter("desc");
		String taskName = request.getParameter("taskName");
		LocalDate localstDate = LocalDate.parse(request.getParameter("startdate"));
		LocalDate localenDate = LocalDate.parse(request.getParameter("enddate"));
		LocalDateTime startDate = localstDate.atStartOfDay();
		LocalDateTime endDate = localenDate.atStartOfDay();
		String[] taskUsers = request.getParameterValues("users");
		Project project = projectService.getProjectById(Long.parseLong(projectId));
		if (project == null || taskDescription == null || taskUsers.length < 1 ) {
			response.sendRedirect("project");
		} else {
			List<Long> listUser = new ArrayList<Long>();
			for (int i = 0; i < taskUsers.length; i++) {
				if (taskUsers[i] != "") {
					listUser.add(Long.parseLong(taskUsers[i]));
				}	
			}
			
			taskService.addTask(
				new Task(taskName, taskDescription, project.getId(), TaskStatus.New.getValue(), startDate, endDate), listUser);
			response.sendRedirect("project?projectId="+project.getId());
		}
	}

}
