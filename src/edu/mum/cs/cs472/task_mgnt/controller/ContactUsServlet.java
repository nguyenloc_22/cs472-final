package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.model.Contact;
import edu.mum.cs.cs472.task_mgnt.service.IContactService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/contact-us")
public class ContactUsServlet extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	
	private IContactService contactService;
	private Logger logger;
	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactUsServlet() {
        super();
        
        contactService = ServiceFactory.getInstance().getContactService();
        logger = Logger.getLogger(this.getClass().getCanonicalName());
        logger.debug("ContactUs servlet -> initialized.");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {
		
		super.doGet(request, response);

		String targetJspPath = "/jsp/page/contact-us.jsp";
		
		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {

		String name = request.getParameter("txtName");
		String gender = request.getParameter("rGender");
		String category = request.getParameter("selCategory");
		String message = request.getParameter("txtMessage");
		
		Contact contact = new Contact(name, gender, category, message);
		contactService.addContact(contact);
				
		logger.debug("doPost --> ");
		doGet(request, response);
	}

}
