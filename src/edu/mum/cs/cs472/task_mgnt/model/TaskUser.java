package edu.mum.cs.cs472.task_mgnt.model;

public class TaskUser extends BaseModel<Long> {
	
	private final String TableName = "task_user";
	private final String[] ColumnNames = { "id", "userId", "taskId" };
	
	private long id;
	private long taskId;
	private long userId;
	
	public TaskUser() { }

	public TaskUser(long taskId, long userId) {
		super();
		this.taskId = taskId;
		this.userId = userId;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getTaskId() {
		return taskId;
	}
	
	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		
		return String.format("UserProject: {"
				+ "id: %d,"
				+ "userId: %d,"
				+ "taskId: %d}", 
				id, userId, taskId);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = Long.parseLong(data[0].toString());
		userId = Long.parseLong(data[1].toString());
		taskId = Long.parseLong(data[2].toString());
	}
	
	@Override
	public Object[] getData() {
		return new Object[] { id, userId, taskId };
	}

}
