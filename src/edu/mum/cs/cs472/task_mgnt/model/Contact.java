package edu.mum.cs.cs472.task_mgnt.model;

// This model is created to test the DAO
// It's safety to be removed

public class Contact extends BaseModel<Long> {
	
	private final String TableName = "contacts";
	private final String[] ColumnNames = { 
	"contacts_id", "customer_name", "gender", "category", "message" };
	
	private long id;
	private String name;
	private String gender;
	private String category;
	private String message;
	
	public Contact() {
		
	}
	
	public Contact(
			String name, 
			String gender, 
			String category, 
			String message) {
		
		super();
		
		this.name = name;
		this.gender = gender;
		this.category = category;
		this.message = message;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return String.format("Contact {"
				+ "CustmerName: %s,"
				+ "Gender: %s,"
				+ "Category: %s,"
				+ "..."
				+ "}", 
				name, gender, category);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public String getIdColumnName() {
		return "contacts_id";
	}
	
	@Override
	public void loadData(Object[] data) {
		id = new Long((int)data[0]);
		name = data[1].toString();
		gender = data[2].toString();
		category = data[3].toString();
		message = data[4].toString();
	}
	
	@Override
	public Object[] getData() {
		return new Object[] {
			id, name, gender, category, message
		};
	}
	
}
