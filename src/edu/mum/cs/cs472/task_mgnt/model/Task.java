package edu.mum.cs.cs472.task_mgnt.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Task extends BaseModel<Long> {
	
	private final String TableName = "task";
	private final String[] ColumnNames = { 
	"id", "name", "description", "projectId", "status", "createdDate", "startDate", "endDate"};
	
	private long id;
	private String name;
	private String description;
	private Long projectId;
	private int status;
	private LocalDateTime createdDate;
	private LocalDateTime startDate;
	private LocalDateTime endDate;

	public Task() { }

	public Task(String name, String description, Long projectId, int status) {
		super();
		this.name = name;
		this.description = description;
		this.projectId = projectId;
		this.status = status;
		this.createdDate = LocalDateTime.now();
	}

	public Task(String name, String description, Long projectId, int status, LocalDateTime startDate, LocalDateTime endDate) {
		super();
		this.name = name;
		this.description = description;
		this.projectId = projectId;
		this.status = status;
		this.createdDate = LocalDateTime.now();
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return String.format("Task: {"
				+ "id: %s,"
				+ "title: %s,"
				+ "description: %s,"
				+ "status: %d"
				+ "}", id, name, description, status);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = (long)(int)data[0];
		name = data[1].toString();
		description = data[2].toString();
		projectId = (long)(int)data[3];
//		long projectId = (long)(int)data[3];
		// TODO
//		project = new Project();
//		project.setId(projectId);
		
		status = (int)data[4];
		Timestamp sqlDate = (Timestamp)data[5];
		createdDate = sqlDate.toLocalDateTime();
		
		sqlDate = (Timestamp)data[6];
		startDate = sqlDate.toLocalDateTime();
		
		sqlDate = (Timestamp)data[7];
		endDate = sqlDate.toLocalDateTime();
		
	}
	
	@Override
	public Object[] getData() {
		return new Object[] {
//			id, name, description, project.getId(), status, createdDate
			id, name, description, projectId, status, createdDate, startDate, endDate
		};
	}
}
