<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <input type="hidden" id="currentRoute" value="${ currentRoute }">
      <a class="navbar-brand" href="./index.jsp"> CS472-WAP :::Project Management</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item item-index">
            <a class="nav-link" href="./index.jsp">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item item-about-us">
            <a class="nav-link" href="./about-us">About</a>
          </li>
          <li class="nav-item item-project">
            <a class="nav-link" href="./project">Projects</a>
          </li>
          <li class="nav-item item-contact-us">
            <a class="nav-link" href="./contact-us">Contact</a>
          </li>
          <li class="nav-item item-logout">
            <a class="nav-link" href="./logout">Logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
