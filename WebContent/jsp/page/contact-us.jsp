<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />

  <link 
    href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx" 
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/page-project.css">
  <title>CS472-WAP ::: Task management!</title>
</head>
<body>
<header>
<%@ include file="../fragment/header.jsp" %>
</header>

<section class="page-content container" style="max-width: 720px">

  <h2>Contact us!!!</h2>
  <form id="frm-contact" method="POST" action="./contact-us" class="needs-validation">
    <!-- Name -->
    <div class="form-group">
      <label for="txtName">*Name</label> 
      <input type="text" id="txtName" name="txtName"
        class="form-control" aria-describedby="nameIdHelp" 
        placeholder="Your name" value="${ param.txtName }">
      <span class="error">${ txtNameError }</span>
    </div>
    
    <!-- Gender -->
    <div class="form-group">
      <label>*Gender</label>

      <div>
        <input id="gMale" type="radio" name="rGender" value="male" 
          ${ 'male' == param.rGender ? 'checked' : '' } >
        <label for="gMale">&nbsp;Male</label>&nbsp;&nbsp;
         
        <input id="gFeMale" type="radio" name="rGender" value="female"
          ${ 'female' == param.rGender ? 'checked' : '' } >
        <label for="gFeMale">&nbsp;Female</label>&nbsp;&nbsp;
         
        <input id="gOther" type="radio" name="rGender" value="other"
          ${ 'other' == param.rGender ? 'checked' : '' } >
        <label for="gOther">&nbsp;Other</label>&nbsp;&nbsp;
      </div>
      <span class="error">${ rGenderError }</span>
    </div>
    
    <!-- Category -->
    <div class="form-group">
      <label for="selCategory">*Category</label> 
      <select id="selCategory" name="selCategory" class="form-control" required>
        <option value="feedback" ${ 'feedback' == param.selCategory ? 'selected' : '' }>Feedback</option>
        <option value="inquiry" ${ 'inquiry' == param.selCategory ? 'selected' : '' }>Inquiry</option>
        <option value="complaint" ${ 'complaint' == param.selCategory ? 'selected' : '' }>Complaint</option>
      </select>
      <span class="error">${ selCategoryError }</span>
    </div>
    
    <!-- Message -->
    <div class="form-group">
      <label for="txtMessage">*Message</label>
      <textarea class="form-control" id="txtMessage"
        name="txtMessage" placeholder="Enter your message">${ param.txtMessage }</textarea>
      <span class="error">${ txtMessageError }</span>
    </div>
    
    <button type="submit" id="btnSubmit" class="btn btn-primary">Submit</button>
  </form>
</section>

<footer class="page-footer font-small blue bg-primary">
<%@ include file="../fragment/footer.jsp" %>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="js/app.js"></script>
</body>
</html>
